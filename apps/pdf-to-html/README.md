# pdf-to-html

Express + Puppeteer based microservice for rendering a html document as a PDF.

## Running the application

Start the application by running the following command from the root of the
repository.

```bash
nx serve pdf-to-html
```

## Make a PDF

```bash
curl -XPOST \
  -H "Content-Type: application/json" \
  -d '{"bodyHtml": "<h1>Hello ngVikings!</h1>"}' \
  --output test_pdf.pdf \
  http://localhost:3333/   
```

## How it works

The application exposes a single endpoint that responds to a HTTP post
containing a json object containing a single `bodyHtml` property (another
iteration of this API had support for supplying header and footer content).

The contents of the `bodyHtml` property are written to a temporary file which
is then opened in Chrome headless by using the puppeteer APIs. Once all
external resources have been loaded Chrome is instructed to export the page as
a PDF document with A4 page size.

The PDF data is then returned to the HTML client and temporary files are cleaned up.

## Potential improvements

* Fix the naming: This was clearly supposed to be called the `html-to-pdf`
  service but I didn't notice until after I created the `push-dont-pull`
  branch and don't fancy navigating that rebase (if it works, don't fix it).
* Add in the possibility of header and footer html again (note: Chrome has some
  bugs regarding header and footer handling).
* Limit concurrent processing: having lots of concurrent browsers running will
  eat up resources, this needs to be mitigated.
* Disable javascript: the intention here was to render complete html therefore
  it should be safe to use the puppeteer api call to disable javascript before
  loading the page.
* Restrict filesystem access: The Chrome browser _only_ needs access to the temp
  html files. Everything else should be restricted if possible to avoid
  malicious HTML including random files from the filesystem.
