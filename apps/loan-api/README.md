# loan-api

Express based backend api. The application exposes three endpoints

1. GET `/loan-api/applications`: Returns a list of all loan applications in the system
1. GET `/loan-api/applications/:id`: Returns a single loan application as identified by the id
1. GET `/loan-api/applications/:id/as-pdf`: Returns the PDF representation of the decision letter for the given application

Calls to the API require a valid JWT.

## Running the application

Start the application by running the following command from the root of the
repository.

```bash
nx serve loan-api
```

## Example requests

```bash
curl \
  -H 'Authorization: bearer eyJhbGciOiJSUzI1NiIsInR5c...' \
  'http://localhost:4200/loan-api/applications'
```

```bash
curl \
  -H 'Authorization: bearer eyJhbGciOiJSUzI1NiIsInR5c...' \
  'http://localhost:4200/loan-api/applications/1'
```

```bash
curl \
  -H 'Authorization: bearer eyJhbGciOiJSUzI1NiIsInR5c...' \
  --output test_pdf.pdf \
  'http://localhost:4200/loan-api/applications/1/as-pdf' 
```

## How it works

The endpoints for retrieving data are backed by a hard-coded in-memory array
for read only operation.

The endpoint for retrieving the PDF representation is a little more involved
as it first calls the `pdf-renderer` application to retrieve the HTML 
representation of the PDF to be generated and then forwards the result to the
`pdf-to-html` service to get the PDF representation to be returned to the
client.

## Potential improvements

* Add endpoints for editing data: This would round off the demo to make case
  handling system fully interactive.
* Add error handling: Any errors when orchestrating the PDF generation should
  be propagated appropriately.
