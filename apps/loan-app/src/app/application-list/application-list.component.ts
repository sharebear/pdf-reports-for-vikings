import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { LoanApplication } from '@pdf-reports-for-vikings/loan-model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'loan-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicationListComponent {
  applications$: Observable<LoanApplication[]>;

  constructor(http: HttpClient) {
    this.applications$ = http.get<LoanApplication[]>('/loan-api/applications');
  }

}
