# loan-app

Example case-handling application. This is supposed to be as small and simple
as possible while demonstrating usage of the `decision-letter` component in an
interactive case-handling scenario. The `Download PDF` button also provides a
convenience for generating the PDF without resorting to curl or other http
clients.

## Running the application

Start the application by running the following command from the root of the
repository.

```bash
nx serve loan-app
```

## Accessing the application

As this is a standard Angular application it is available on the default port
`http://localhost:4200/`. On first load you will be redirected to the Keycloak
login page where the username `viking` and password `insecure` should get you
redirected back to the application.

## Potential improvements

* Implement editing of case data in the frontend. This will demonstrate how the
  `decision-letter` component updates in realtime when used in the frontend but
  can still be used statically in the rendering application.
* UI fixes
* More semantic markup
