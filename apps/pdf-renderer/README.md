# pdf-renderer

[Angular Universal](https://angular.io/guide/universal) application with a
single route `pdf/application-decision/:id` that retrieves the case data from
the loan-api and passes that data to the `decision-letter` component.

The intention then is that the resulting html can be passed straight to the
`pdf-to-html` service to geneate a PDF document.

## Running the application

Start the application by running the following command from the root of the
repository.

```bash
nx serve-ssr pdf-renderer
```

## Generate HTML (master branch)

```bash
curl -H "Authorization: bearer eyJhbGciOiJ..." \
  http://localhost:3334/pdf/application-decision/1
```

On the `master` branch this service requires a JWT as input that it uses when
calling the loan-api.

## Generate HTML (master branch)

```bash
curl -XPOST \
  -H "Content-Type: application/json" \
  -d '{"id": "1", "subject": "CAR", "status": "APPROVED", "letterTexts": {}}' \
  http://localhost:3334/pdf/application-decision
```

On the `push-dont-pull` branch the route no longer requires an id
parameter as the case data is provided through the POST body, this also removes
the need for an authentication token.

## Potential improvements

* Add error handling: Any errors when rendering the page should result in a
  HTTP error being returned to the client.
* Experiment with authentication so that the regular angular server could be
  used interactively during development. In theory it should be possible to
  configure the Keycloak client only when running as a regular Angular app,
  this would potentially be useful during development.
