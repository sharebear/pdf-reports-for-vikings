import { Injectable, Inject, Optional } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Request } from 'express';
import { REQUEST } from '@nguniversal/express-engine/tokens';

@Injectable()
export class AuthorizationHeaderInterceptor implements HttpInterceptor {

  constructor(@Optional() @Inject(REQUEST) protected expressRequest?: Request) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const headerContent = this.expressRequest.header("Authorization");
    return next.handle(request.clone({
      headers: request.headers.set("Authorization", headerContent)
    }));
  }
}
