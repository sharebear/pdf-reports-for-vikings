# Pdf Reports For Vikings

This project contains a full proof of concept for how it is possible to
generate PDF documents using Angular for layout and population of data.

A walkthrough of the important bits of code along with full explanation
of the architecture was presented at ngVikings 2020.

[![ngVikings talk on YouTube](docs/youtube.png)](https://youtu.be/B4mqeXbNGkY?t=7211 "ngVikings talk on YouTube")

## Quickstart

The following steps will start all of the services locally on your machine.

### 1. Install nx

```bash
npm install -g @nrwl/cli
```

This project is structured as a monorepo managed with [nx](https://nx.dev/angular)
so using the nx client locally is the simplest way to get started.

### 2. Install npm dependencies

```bash
npm install
```

### 3. Start Keycloak

```bash
docker-compose up
```

For authentication this proof of concept uses
[Keycloak](https://www.keycloak.org/) running in a docker container for
identity management.

On first startup the loan-application realm is created based on the data in
`keycloak/loan-application-realm.json` file.

To authenticate to the admin console use root/insecure as the credentials.
To authenticate to the application use viking/insecure as the credentials.

### 4. Start pdf-to-html service

```bash
nx serve pdf-to-html
```

This is the service that receives html in a post request, and renders it as a
PDF using chrome headless via puppeteer. More info available in the [pdf-to-html
readme](apps/pdf-to-html/README.md).

### 5. Start pdf-renderer service

```bash
nx serve-ssr pdf-renderer
```

This is the Angular Universal app serving the markup to use as a source for the
PDF. For more information see the [pdf-renderer readme](apps/pdf-renderer/README.md).

### 6. Start loan-api service

```bash
nx serve loan-api
```

This is a very thin stub API to provide data to applications and orchestrate 
the document generation. For more information see the [loan-api readme](apps/loan-api/README.md).

### 7. Start loan-app service

```bash
nx serve loan-app
```

Stub case handling application to show the PDF component being rendered in the
frontend. Some more detailed information can be found in the [loan-app readme](apps/loan-app/README.md).

### 8. Open the application

Now the frontend application should be available at http://localhost:4200/
changes to any of the code should be hot-reloaded. Login with the username
`viking` and password `insecure`.

## Alternative branch

In the talk I present two methods of architecting the solution. The first, by
propagating authentication tokens through the Angular Universal application 
is represented by the `master` branch in this repository.

The second strategy where we push data to the Angular Universal application
with a post request can be found on the `push-dont-pull` branch which builds
on top of `master`.

WARNING: When I make changes on `master` I will rebase and force push the
`push-dont-pull` branch to keep the history of changes there clean over time.

## Something's missing/broken

Check the [issues](https://gitlab.com/sharebear/pdf-reports-for-vikings/-/issues)
there are some rough edges I didn't fix before the presentation.
