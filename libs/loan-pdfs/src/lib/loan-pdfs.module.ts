import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DecisionLetterComponent } from './decision-letter/decision-letter.component';

@NgModule({
  imports: [CommonModule],
  declarations: [DecisionLetterComponent],
  exports: [DecisionLetterComponent],
})
export class LoanPdfsModule {}
