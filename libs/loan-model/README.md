# loan-model

This library contains models shared between the `loan-app` application and
`loan-api` backend. Currently the model is also shared with the
`decision-letter` component that should get it's own decoupled model.
